export const sliderItems = [
	{
		id: 1,
		img: "https://wallpapercave.com/wp/wp8484405.jpg",		
		title: "LEVEL UP YOUR GAMING EXPERIENCE!",
		desc: "UP YOUR GAME THIS SUMMER! GET FLAT 30% OFF ON NEW RELEASES AND BESTSELLERS!",
		bg: "f5fafd",
	},
	{
		id: 2,
		img: "https://wallpapercave.com/wp/wp8484399.jpg",
		title: "READY TO GAME?",
		desc: "GET 30% OFF NEW ARRIVALS!",
		bg: "ffffff",
	},
	{
		id: 3,
		img: "https://wallpaperaccess.com/full/172756.jpg",
		title: "HOT SUMMER, HOTTER DEALS!",
		desc: "GET FLAT 30% OFF ON NEW GAMING ARRIVALS.",
		bg: "ffffff",
	}
]

export const categories = [
	{
		id: 1,
		img: "https://i.pinimg.com/564x/a2/94/b5/a294b593769b6bf61b55c152032e7459.jpg",
		title: "GAMING ACCESSORIES",
		cat: "accessories"
	},
	{
		id: 2,
		img: "https://c4.wallpaperflare.com/wallpaper/677/807/774/pc-gaming-computer-pc-cases-technology-wallpaper-preview.jpg",
		title: "SYSTEM UNITS",
		cat: "units"
	},
	{
		id: 3,
		img: "https://i5.walmartimages.com/asr/1e0f1cc0-c471-42de-95c3-32f0a056babc.4fbefd22ac1019f7d0d8cd1361a37821.jpeg",
		title: "GAMING CHAIR AND TABLE",
		cat: "set"
	}
]

export const popularProducts = [
	{
		id: 1,
		img: "https://wazy.ly/ecdata/stores/CEMOQK5187/image/data/products/1611078501_Sensei1200x1200-01.png",
	},
	{
		id: 2,
		img: "https://www.adesso.com/wp-content/uploads/2018/03/AKB-640EB-1.jpg",
	},
	{
		id: 3,
		img: "https://www.adesso.com/wp-content/uploads/2018/07/G4-1-1.jpg",
	},
	{
		id: 4,
		img: "https://www.lg.com/us/images/monitors/md08000380/350.jpg",
	},
	{
		id: 5,
		img: "https://jp.images-monotaro.com/Monotaro3/pi/full/mono64548268-230203-02.jpg",
	},
	{
		id: 6,
		img: "https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T2/images/I/5150ly7RPpL._AC_SX679_.jpg",
	},
	{
		id: 7,
		img: "https://l33t-gaming.com/wp-content/uploads/2021/01/160401_01.png",
	},
	{
		id: 8,
		img: "https://jp.images-monotaro.com/Monotaro3/pi/full/mono63637018-220118-02.jpg",
	}
]


