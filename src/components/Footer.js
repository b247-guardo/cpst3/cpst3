import React from 'react';
import SendIcon from '@mui/icons-material/Send';
import styled from 'styled-components';
import Swal from 'sweetalert2';
import { useState } from 'react';
import { mobile } from '../responsive';

const Container = styled.div`
	height: 60vh;
	background-color: #fcf5f5;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
`;
const Title = styled.h1`
	font-size: 70px;
	margin-bottom: 20px;
	${mobile({ fontSize: "40px"})}
`;
const Description = styled.div`
	font-size: 24px;
	font-weight: 300;
	margin-bottom: 20px;
	${mobile({ textAlign: "center"})}
`;
const InputContainer = styled.div`
	width: 50%;
	height: 40px;
	background-color: white;
	display: flex;
	justify-content: space-between;
	border: 1px solid lightgray;
	${mobile({ width: "80%"})}
`;

const Input = styled.input`
	border: none;
	flex: 8;
	padding-left: 20px;
`;

const Button = styled.button`
	flex: 1;
	border: none;
	background-color: teal;
	color: white;
	cursor: pointer;
`


export default function Footer(){

	const [email, setEmail] = useState("");

  	const handleSubmit = (e) => {
	    e.preventDefault();
	    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
				method: 'POST',
				body: JSON.stringify({
					email: email
				}),
				headers: {
					'Content-Type': 'application/json'
				}
			})
			.then(res => res.json())
			.then(data => {
				if (data === true) {
					Swal.fire({
						title: "Success!",
						icon: "success",
						text: "We will give you timely updates from your favorite products."
					})
					setEmail("");		
				} 	else {
					Swal.fire({
						title: "Failed!",
						icon: "error",
						text: "Please try again."
					})
				}		
			}) 
		}

	return (
		<Container>
			<Title>GameXtreme</Title>
			<Description>Be the first to learn about promotions, special events, new arrivals and more!</Description>
			<form onSubmit={handleSubmit} style={{width: '100%', display: 'flex', justifyContent: 'center'}}>
			<InputContainer>
				<Input value={email} onChange={(e)=> setEmail(e.target.value)} placeholder="Your email" />
				<Button type="submit">
					<SendIcon />
				</Button>
			</InputContainer>
			</form>
		</Container>
	)
}