import styled from 'styled-components';
import { mobile } from '../responsive';

const Container = styled.div`
	height: 30px;
	background-color: black;
	color: white;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 20px;
	font-weight: 500;
	${mobile({fontSize: "14px"})}
`
export default function Announcement() {
	return (
		<Container>
			Super Deal! Free Shipping on Orders over P50
		</Container>
	)
}