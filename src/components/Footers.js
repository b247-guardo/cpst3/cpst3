import React from 'react';
import styled from 'styled-components';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import RoomIcon from '@mui/icons-material/Room';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import { mobile } from '../responsive';

const Container = styled.div`
	display: flex;
	${mobile({ flexDirection: "column"})}
`
const Left = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	padding: 20px;
`
const Logo = styled.h1``

const Desc = styled.p`
	margin: 20px 0px;
`;

const SocialContainer = styled.div`
	display: flex;
`;

const SocialIcon = styled.div`
	width: 40px;
	height: 40px;
	border-radius: 50%;
	color: white;
	background-color: #${props=>props.color};
	display: flex;
	align-items: center;
	justify-content: center;
	margin-right: 20px;
`;

const Center = styled.div`
	flex: 1;
	padding: 20px;
	${mobile({ display: "none"})}
`

const Title = styled.h3`
	margin-bottom: 30px;
`
const List = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
	display: flex;
	flex-wrap: wrap;
`;
const ListItem = styled.li`
	width: 50%;
	margin-bottom: 10px;
`
const Right = styled.div`
	flex: 1;
	padding: 20px;
	${mobile({ backgroundColor: "#fff8f8"})}
`
const ContactItem = styled.div`
	margin-bottom: 20px;
	display: flex;
	align-items: center;
`
const Payment = styled.img`
	width: 50%;
`

export default function Footers() {
	return (
		<Container>
			<Left>
				<Logo>GameXtreme</Logo>
				<Desc>Our store helps you to fulfill your gaming and gadget needs. We sell next-generation consoles, games, gadgets, and various peripherals for better gaming experience. For latest updates, check us through our social media accounts.</Desc>
				<SocialContainer>
					<SocialIcon color="3B5999">
						<FacebookIcon />
					</SocialIcon>
					<SocialIcon color="E4405F">
						<InstagramIcon />
					</SocialIcon>
					<SocialIcon color="55ACEE">
						<TwitterIcon />
					</SocialIcon>
					<SocialIcon color="E60023">
						<PinterestIcon />
					</SocialIcon>
				</SocialContainer>
			</Left>
			<Center>
				<Title>Useful Link</Title>
				<List>
					<ListItem>Home</ListItem>
					<ListItem>Cart</ListItem>
					<ListItem>Accessories</ListItem>
					<ListItem>My Account</ListItem>
					<ListItem>Order Tracking</ListItem>
					<ListItem>Wishlist</ListItem>
					<ListItem>Terms</ListItem>
				</List>
			</Center>
			<Right>
				
				<Title>Contact</Title>
				<ContactItem><RoomIcon style={{marginRight:"10px"}} />
				6000 Cebu City, Cebu, Philippines
				</ContactItem>
				<ContactItem><PhoneIcon style={{marginRight:"10px"}}/>
				+032 462 2348
				</ContactItem>
				<ContactItem><EmailIcon style={{marginRight:"10px"}}/>
				contact@gamextreme.com.ph
				</ContactItem>
				<Payment src="https://www.citypng.com/public/uploads/preview/visa-mastercard-discover-american-express-icons-21635415958rgxaunvs7z.png" />
			</Right>
		</Container>
	)
}