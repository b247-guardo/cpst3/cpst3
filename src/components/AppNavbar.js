import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Navbar from 'react-bootstrap/Navbar';
import React, { useEffect, useState, useContext } from 'react';
import styled from 'styled-components'
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { Search } from '@mui/icons-material';
import Badge from '@mui/material/Badge';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import { mobile } from '../responsive';

  const Container = styled.div`
      height: 60px;
      ${mobile({ height: "60px"})}
  `;

  const Wrapper = styled.div`
      padding: 10px 20px;
      display: flex;
      align-items: center;
      justify-content: space-between;
      ${mobile({ padding: "10px 0px"})}
  `;

  const Left = styled.div`
      flex: 1;
      display: flex;
      align-items: center;
  `;

  const Language = styled.span`
      font-size: 14px;
      cursor: pointer;
      text-decoration: none;
      ${mobile({ display: "none"})}
  `;

  const SearchContainer = styled.div`
      border: 0.5px solid lightgray;
      display: flex;
      align-items: center;
      margin-left: 25px;
      padding: 5px;
      ${mobile({ marginLeft: "10px"})}
  `;

  const Input = styled.input`
      border: none;
      ${mobile({ width: "50px"})}
  `;

  const Center = styled.div`
      flex: 1;
      text-align: center;
  `;

  const Logo = styled.h1`
      font-weight: bold;
      font-size: 30px;
      color: black;
      ${mobile({ fontSize: "20px"})}
  `;
  const Right = styled.div`
      flex: 1;
      display: flex;
      align-items: center;
      justify-content: flex-end;
      ${mobile({ flex:1, justifyContent: "center"})}
  `;

  const MenuItem = styled.div`
      font-size: 14px;
      cursor: pointer;
      margin-left: 25px;
      ${mobile({ fontSize: "10px", marginLeft: "5px"})}
  `

export default function AppNavbar() {

  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const handleClick = () => {
    navigate('/cart');
  };

  return (

    <Navbar collapseOnSelect bg="light" variant="light" expand="lg">
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Container>
            <Wrapper>
              <Left>
                <Language as={NavLink} to="/">HOME</Language>
                <SearchContainer>
                  <Input placeholder="Search"/>
                  <Search style={{color: "gray", fontsize: 16}}/>
                </SearchContainer>
              </Left>
              <Navbar.Brand><Center><Logo as={NavLink} to="/">GameXtreme</Logo></Center></Navbar.Brand>
              <Right>
                { (user.id !== null) ? (
                  <>
                  { (user.isAdmin) ? (
                  <MenuItem as={NavLink} to="/admin-dashboard">ADMIN
                  </MenuItem>
                  ) : null }
                  <MenuItem as={NavLink} to="/active-product">PRODUCTS</MenuItem>
                  <MenuItem as={NavLink} to="/logout">LOGOUT</MenuItem>
                  </>
                  ) : (
                  <>
                  <MenuItem as={NavLink} to="/register">REGISTER</MenuItem>
                  <MenuItem as={NavLink} to="/">SIGN IN</MenuItem>
                  </>
                  )}
                <MenuItem>
                  <Badge onClick={handleClick} badgeContent={0} color="primary">
                    <ShoppingCartOutlinedIcon/>
                  </Badge>
                </MenuItem>
              </Right>
            </Wrapper>
          </Container>
          </Nav>
      </Navbar.Collapse>
    </Navbar>

  );
}
