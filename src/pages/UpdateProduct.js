import React from 'react';
import { useState, useContext } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';
import Footers from '../components/Footers';
import styled from 'styled-components';
import { mobile } from '../responsive';
import UserContext from '../UserContext';

const Container = styled.div`
	width: 100%;
	height: 100vh;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: white;
	${mobile({width: "75%"})}
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
`;

const Form = styled.form`
	display: flex;
	flex-direction: column;
`;

const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 20px 10px 0px 0px;
	padding: 10px;
`;

const DescriptionInput = styled.textarea`
  	height: 100px;
  	padding: 10px;
  	margin: 20px 10px 0px 0px;
  	min-width: 40%;
`;

const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	margin-top: 25px;
`;

export default function UpdateProduct(){

	const {user} = useContext(UserContext);
	const { id } = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();

	const handleUpdate = (e) => {
		e.preventDefault();
		console.log(id);
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
			method: 'PUT',
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			}),
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire({
					title: "Product successfully updated!",
					icon: "success",
					text: "Thank you!"
				})
				navigate('/admin/retrieve');
			} else {
				Swal.fire({
					title: "Error in updating product",
					icon: "error",
					text: "Please try again."
				})
			}
		})
		.catch(error => {
			console.log(error);
			Swal.fire({
				title: "Error in updating product",
				icon: "error",
				text: "Please try again."
			});
		});

		setName("");
		setDescription("");
		setPrice("");

	};


	return (
		<Container>
			<Wrapper>
				<Title>UPDATE A PRODUCT</Title>
				<Form onSubmit={(e) => handleUpdate(e)}>
					<Input type="text" value={name} onChange={e => setName(e.target.value)} placeholder="Enter new product name" required/>
					<DescriptionInput type="text" value={description} onChange={e => setDescription(e.target.value)} placeholder="Enter new product description" required/>
					<Input type="text" value={price} onChange={e => setPrice(e.target.value)} placeholder="Enter new product price" required/>
					<Button>UPDATE</Button>
				</Form>
			</Wrapper>
		</Container>
	)
}