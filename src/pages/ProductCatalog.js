import styled from 'styled-components';
import Announcement from '../components/Announcement';
import Products from '../components/Products';
import Footer from '../components/Footer';
import Footers from '../components/Footers';
import { mobile } from '../responsive';
import { useState } from 'react';
import { useLocation, Location } from 'react-router-dom';

const Container = styled.div``

const Title = styled.h1`
	margin: 50px;
	text-align: center;
`

export default function ProductCatalog() {
	
	return(
		<Container>
		<Announcement />
		<Title>Gaming Products</Title>
		<Products />
		<Footer/>
		<Footers/>
		</Container>
	)
}