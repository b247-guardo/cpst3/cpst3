import styled from 'styled-components';
import { useNavigate, Navigate, Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { mobile } from '../responsive';


const Container = styled.div`
	width: 100%;
	height: 100vh;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: white;
	${mobile({width: "75%"})}
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
`;

const Form = styled.form`
	display: flex;
	flex-direction: column;
`;

const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 10px 0px;
	padding: 10px;
`;

const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	margin-bottom: 10px;
`;

const Links = styled.a`
	margin: 5px 0px;
	font-size: 12px;
	text-decoration: underline;
	cursor: pointer;
`;


export default function Login(){

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);

	function authenticate(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.access);
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to GameXtreme! Enjoy shopping!'
				})
				navigate('/');
			} else {
				Swal.fire({
					title: 'Login Failed',
					icon: 'error',
					text: 'Please, check your login details and try again!'
				})
			}
		});

		setEmail("");
		setPassword("");

		const retrieveUserDetails = (token) => {

			fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			});
		};
	};



	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
		<Container>
			<Wrapper>
				<Title>SIGN IN</Title>
				<Form onSubmit={(e) => authenticate(e)}>
					<Input type="text" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Username" required/>
					<Input type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" required/>
					<Button type="submit">LOGIN</Button>
					<Links as={Link} to="/forgotpassword">FORGOT PASSWORD?</Links>
					<Links as={Link} to="/register">CREATE A NEW ACCOUNT</Links>
				</Form>
			</Wrapper>
		</Container>
	)
}