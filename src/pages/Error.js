import { Link } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';
import { Navigate, useNavigate } from 'react-router-dom';

const Container = styled.div`
	display: flex;
	flex-direction: column;
	height: 90vh;
	align-items: center;
	justify-content: center;
	font-size: 20px;
	font-weight: 500;
`;
const Image = styled.img`
	display: flex;
	height: 60%;
	width: 30%;
`
const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	margin: 10px;
`;

const Title = styled.h1`
	font-size: 50px;
	font-weight: 300;
`
export default function Error(){

	const navigate = useNavigate();
	const handleClick = () => {
		navigate('/');
	}
	return(
		<>
		<Container>
			<Image src="https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-1932.jpg?w=826&t=st=1680556868~exp=1680557468~hmac=2ecdda677c1d9dd2638e9c0b9788fab7423ff32976881de8735a5b7ebf806515" />
			<Title>404. That's an error</Title>
			<p style={{textAlign: "center", margin: "1px", padding: "1px"}}>The requested URL /badpage was not found on this server.</p>
			<p style={{textAlign: "center", margin: "1px", padding: "1px"}}>That's all we know</p>
			<Button onClick={handleClick} style={{fontSize: "25px"}}>Go back to the homepage.</Button>
		</Container>
		</>
	);
};