import styled from 'styled-components';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { mobile } from '../responsive';

const Container = styled.div`
	width: 100%;
	height: 100vh;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: white;
	${mobile({width: "75%"})}
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
`;

const Form = styled.form`
	display: flex;
	flex-direction: column;
`;

const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 20px 10px 0px 0px;
	padding: 10px;
`;

const Agreement = styled.span`
	font-size: 12px;
	margin: 20px 0px;
`;

const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
`;

export default function Register(e){

	const {user} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [username, setUserName] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			body: JSON.stringify({
				email: email
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					body: JSON.stringify({
						email: email,
						password: password1,
						firstName: firstName,
						lastName: lastName,
						username: username,
						mobileNo: mobileNo
					}),
					headers: {
						'Content-Type': 'application/json'
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire({
						title: "Registration successful",
						icon: "success",
						text: "Thank you for registering at GameXtreme!"
					})
					navigate("/");
				});
			};
		});

		setEmail("");
		setPassword1("");
		setPassword2("");
		setUserName("");
		setFirstName("");
		setLastName("");
		setMobileNo("");
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both password matches

		if((email !== "" && password1 !== "" && password2 !== "" && lastName !== "" && firstName !== "") && (password1 === password2) && (password1.length && password2.length >= 8) && (mobileNo.length >= 11)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNo]);

	return (
		(user.id !== null) ?
		<Navigate to="/login" />
		:
		<Container>
			<Wrapper>
				<Title>CREATE AN ACCOUNT</Title>
				<Form onSubmit={(e) => registerUser(e)}>
					<Input type="text" value={firstName} onChange={e => setFirstName(e.target.value)} placeholder="First Name" required/>
					<Input type="text" value={lastName} onChange={e => setLastName(e.target.value)} placeholder="Last Name" required/>
					<Input type="text" value={username} onChange={e => setUserName(e.target.value)} placeholder="Username" required/>
					<Input type="text" value={mobileNo} onChange={e => setMobileNo(e.target.value)} placeholder="Mobile Number" required/>
					<Input type="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" required/>
					<Input type="password" value={password1} onChange={e => setPassword1(e.target.value)} placeholder="Password" required/>
					<Input type="password" value={password2} onChange={e => setPassword2(e.target.value)} placeholder="Confirm Password" required/>
					<Agreement>
						By creating an account, I consent to the processing of my personal data in accordance with the <b>PRIVACY POLICY</b>
					</Agreement>
					<Button>CREATE</Button>
				</Form>
			</Wrapper>
		</Container>
	)
}