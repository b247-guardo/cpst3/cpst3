import styled from 'styled-components';
import Products from '../components/Products';
import Footer from '../components/Footer';
import Footers from '../components/Footers';
import { mobile } from '../responsive';
import { useState } from 'react';
import { useLocation, Location, useNavigate, Navigate } from 'react-router-dom';

const Container = styled.div`
	width: 100%;
	height: 100vh;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: #fcf5f5;
	${mobile({width: "75%"})}
`;

const Title = styled.h1`
	margin: 20px;
	text-align: center;
`

const FilterContainer = styled.div`
	display: flex;
	justify-content: space-between;
	margin: 0px 30px;
`
const Filter = styled.div`
	margin: 20px;
	${mobile({width: "0px 20px", display: "flex", flexDirection: "column"})}
`;
const FilterText = styled.span`
	font-size: 20px;
	font-weight: 600;
	margin-right: 20px;		
	${mobile({marginRight: "0px"})}
`;
const Select = styled.select`
	padding: 10px;
	margin-right: 20px;
	${mobile({margin: "10px 0px"})}
`
const Option = styled.option`
	
`;

export default function AdminDashboard() {
	const location = useLocation();
	const navigate = useNavigate();
	const cat = location.pathname.split("/")[2];
	const [filters,setFilters] = useState({});
	const [sort, setSort] = useState("newest");

	const handleFilter = (e) => {
		const value = e.target.value;
		setFilters({
			...filters,
			[e.target.name]: value,
		})

		if (value === "create") {
			navigate('/admin/create-product');
		} if (value === "retrieve") {
			navigate('/admin/retrieve');
		} if (value === "update") {
			navigate('/admin/retrieve') 
		} if (value === "activate") {
			navigate('/admin/retrieve')
		}
	};

	return(
		<div>
		<Container>
		<Wrapper>
		<Title>ADMIN SETTINGS</Title>
		<FilterContainer>
			<Filter>
				<FilterText>Options:</FilterText>
				<Select name="options" onChange={handleFilter}>
					<Option value="">--Please select an option--</Option>
					<Option value="create">
						Create Product
					</Option>
					<Option value="retrieve">Retrieve all products</Option>
					<Option value="update">Update Product Information</Option>
					<Option value="activate">Deactivate/Reactivate Product</Option>
				</Select>
			</Filter>
		</FilterContainer>
		</Wrapper>
		</Container>
		<Footer/>
		<Footers/>
		</div>
	)
}