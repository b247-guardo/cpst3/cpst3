import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Link, useNavigate } from 'react-router-dom';
import Footers from '../components/Footers';
import Footer from '../components/Footer';
import Swal from 'sweetalert2';

const Container = styled.div`
  width: 100%;
  height: auto;
  padding: 50px;
  background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #fcf5f5;
  width: 80%;
  height: auto;
  padding: 50px;
`;

const Title = styled.h2`
  font-size: 32px;
  font-weight: 700;
  margin-bottom: 20px;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 30px;
`;

const TableHead = styled.thead`
  background-color: black;
  color: #fff;
`;

const TableBody = styled.tbody``;

const TableRow = styled.tr``;

const TableCell = styled.td`
  padding: 10px;
  text-align: center;
  border-bottom: 1px solid #ddd;
`;

const TotalAmount = styled.p`
  font-size: 24px;
  margin-top: 20px;
`;

const CheckoutButton = styled.button`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin-top: 20px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

const EmptyCart = styled.p`
  font-size: 24px;
  text-align: center;
  margin-top: 30px;
`;

const RemoveItemButton = styled.button`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin-top: 20px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

const ClearCartButton = styled.button`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin-top: 20px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

const ContinueShoppingButton = styled.button`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin-top: 20px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

export default function Cart() {
  const navigate = useNavigate();
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    setCartItems(JSON.parse(localStorage.getItem('cartItems')) || []);
  }, []);

  const handleRemoveItem = (itemId) => {
    const updatedCartItems = cartItems.filter(item => item._id !== itemId);
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
  };

  const handleClearCart = () => {
    localStorage.removeItem('cartItems');
    setCartItems([]);
  };

  const handleClick = () => {
    navigate('/active-product');
  }

  const handleCheckOut = () => {
    Swal.fire({
      title: 'Proceed to checkout?',
      text: 'Take a moment to review your order before proceeding.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, I want to checkout!'
    })
    .then(result => {
      if (result.isConfirmed){
        Swal.fire({
          title: "Thank you for your purchase at GameXtreme!",
          icon: "success",
          text: "You will receive a confirmation email shortly."
        })
      }
    })
  };

  if (cartItems.length === 0) {
    return (
      <div>
      <Container style={{height: "60vh"}}>
        <Wrapper style={{width: "auto"}}>
          <EmptyCart>Your cart is empty.</EmptyCart>
          <ContinueShoppingButton onClick={handleClick}>Continue Shopping</ContinueShoppingButton>
        </Wrapper>
      </Container>
      <Footers />
      </div>
    );
  }

  const totalAmount = cartItems.reduce((total, item) => total + item.total, 0);

  return (
    <div>
      <Container>
        <Wrapper>
          <Title>Shopping Cart</Title>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Product</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Sub-Total</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {cartItems.map(item => (
                <TableRow key={item._id}>
                  <TableCell>{item.name}</TableCell>
                  <TableCell>{item.quantity}</TableCell>
                  <TableCell>P{item.price}</TableCell>
                  <TableCell>P{item.total}</TableCell>
                  <TableCell><RemoveItemButton onClick={() => handleRemoveItem(item._id)}>Remove</RemoveItemButton></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <TotalAmount>Total Amount: P{totalAmount}</TotalAmount>
          <CheckoutButton onClick={handleCheckOut}>Checkout</CheckoutButton>
          <ClearCartButton onClick={handleClearCart}>Clear Cart</ClearCartButton>
          <ContinueShoppingButton onClick={handleClick}>Continue Shopping</ContinueShoppingButton>
        </Wrapper>
      </Container>
      <Footers />
    </div>
  );
}
