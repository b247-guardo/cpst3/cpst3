import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from 'react-bootstrap';
import {mobile} from '../responsive';
import Footers from '../components/Footers';
import Product from '../components/Products';
import UserContext from '../UserContext';
import { useNavigate, useParams } from 'react-router-dom';

const Container = styled.div`
  width: 100%;
  height: auto;
  padding: 50px;
  background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;  
`;

const Wrapper = styled.div`
  width: 80%;
  height: auto;
  padding: 20px;
  background-color: #fcf5f5;
  ${mobile({width: "75%"})}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 600;
  text-align: center;
  margin-bottom: 12px;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;

  th {
    background-color: #fcf5f5;
    padding: 8px;
    font-weight: bold;
    text-align: left;
    border: 1px solid #ccc;
    text-align: center;
  }

  td {
    padding: 8px;
    border: 1px solid #ccc;
  }

  tr:nth-child(even) {
    background-color: #ffffff;
  }

  tr:hover {
    background: linear-gradient(to bottom, #d3d3d3 0%, #ffcc99 100%);
  }
`;

const ActionButton = styled.button`
 background-color: #fff;
 border: 2px solid #000;
 color: #000;
 font-size: 16px;
 padding: 10px 20px;
 cursor: pointer;
 transition: all 0.3s ease;
 margin-top: 20px;

 &:hover {
   background-color: #000;
   color: #fff;
 }

  &:last-child {
    margin-right: 0;
  }
`;

export default function ActiveProducts() {

  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [cartItems, setCartItems] = useState([]);

  const handleViewDetails = (id) => {
    navigate(`/product/${id}`);
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(response => response.json())
      .then(data => setProducts(data.filter(p => p.isActive)))
      .catch(error => console.error(error));
  }, []);

  return (
    <div>
    <Container>
      <Wrapper>
        <Row>
          <Col lg={{ span: 10, offset: 1 }}>
            <Title>ACTIVE PRODUCTS LIST</Title>
            <Table>
              <thead>
                <tr>
                {user.isAdmin ? <th>ID</th> : null}                
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {products.map((product) => (
                  product.isActive && (
                  <tr key={product._id}>
                    {user.isAdmin ? <td>{product._id}</td> : null}
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>P{product.price}</td>
                    <td>
                      <ActionButton onClick={() => handleViewDetails(product._id)}>View Details</ActionButton>
                    </td>
                  </tr>
                  )
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Wrapper>
    </Container>
    <Product />
    <Footers />
    </div>
  );
}
