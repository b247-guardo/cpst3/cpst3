import { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Swal from 'sweetalert2';

const Container = styled.div`
	width: 100%;
	height: 100vh;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: white;
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
	margin-bottom: 10px;
`;

const Form = styled.form`
	display: flex;
	flex-direction: column;
`;

const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	margin-bottom: 10px;
	margin-top: 20px;
`;

const Links = styled.a`
	margin: 10px 0px;
	font-size: 14px;
	text-decoration: underline;
	cursor: pointer;
`;

export default function ForgotPassword() {

  const [email, setEmail] = useState('');
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [error, setError] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			body: JSON.stringify({
				email: email
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "An email has been sent to your email address. Please check your email and follow the instructions to reset your password."
				})
    			setIsSubmitted(true);
  			} else {
  			Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "This email is not yet registered."
				})
  			}
  		});
	}
  

	  return (
	  	<Container>
	  		<Wrapper>
			    <div>
			      {!isSubmitted ? (
			        <Form onSubmit={handleSubmit}>
			          <Title>FORGOT PASSWORD</Title>
			          {error && <div>{error}</div>}
			          <label htmlFor="email">Email:</label>
			          <input
			            type="email"
			            id="email"
			            value={email}
			            onChange={(e) => setEmail(e.target.value)}
			            required
			          />
			          <Button type="submit">Reset Password</Button>
			        </Form>
			      ) : (
			        <div>
			        	<Title>FORGOT PASSWORD</Title>
			        	<p>Please check your email for instructions on resetting your password.</p>
			        	<Links as={Link} to="/">Go back to login</Links>
			        </div>
			      )}
			    </div>
			</Wrapper>
	    </Container>
	  );
	}

