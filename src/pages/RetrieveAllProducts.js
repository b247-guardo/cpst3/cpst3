import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from 'react-bootstrap';
import {mobile} from '../responsive';
import UserContext from '../UserContext';
import Footer from '../components/Footer';
import Footers from '../components/Footers';
import Swal from 'sweetalert2';
import {useNavigate, useParams} from 'react-router-dom';

const Container = styled.div`
	width: 100%;
	height: auto;
  padding: 50px;
	background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;	
`;

const Wrapper = styled.div`
	width: 80%;
	height: auto;
	padding: 20px;
	background-color: #fcf5f5;
	${mobile({width: "75%"})}
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 600;
	text-align: center;
	margin-bottom: 12px;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;

  th {
    background-color: #fcf5f5;
    padding: 8px;
    font-weight: bold;
    text-align: left;
    border: 1px solid #ccc;
    text-align: center;
  }

  td {
    padding: 8px;
    border: 1px solid #ccc;
  }

  tr:nth-child(even) {
    background-color: #ffffff;
  }

  tr:hover {
    background: linear-gradient(to bottom, #d3d3d3 0%, #ffcc99 100%);
  }
`;

const StyledButton = styled(Button)`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin: 2px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;


export default function RetrieveAllProducts() {

  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const {productId} = useParams();
  const [products, setProducts] = useState([]);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(response => response.json())
      .then(data => setProducts(data))
  }, []);

  const handleUpdate = (productId) => {
  	navigate(`/admin/update/${productId}`);
  };
  const handleDeactivate = (productId) => {

  	Swal.fire({
      title: 'Are you sure?',
      text: 'The product will be deactivated.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, deactivate it!'
    })
    .then(result => {
      if(result.isConfirmed){
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            isActive: false
          })
        })
        .then(res => res.json())
        .then(data => {
          setProducts(prevProducts => prevProducts.map(p => p._id === productId ? {...p, isActive: false} : p));
          Swal.fire(
            'Deactivated!',
            'The product has been deactived.',
            'success'
          );
        })
        .catch(error => console.error(error));
      };
    });
  };

  const handleReactivate = (productId) => {
  	Swal.fire({
      title: 'Are you sure?',
      text: 'This product will be reactivated',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reactivate it!'
    })
    .then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            isActive: true
          })
        })
        .then(res => res.json())
        .then(data => {
          setProducts(prevProducts => prevProducts.map(p => p._id === productId ? {...p, isActive: true} : p));
          Swal.fire(
            'Reactivated',
            'The product has been reactivated',
            'success'
          );
        })
        .catch(error => console.error(error));
      };
    });
  };

 return (
 	<div>
     <Container>
       <Wrapper>
         <Row>
           <Col lg={{ span: 10, offset: 1 }}>
             <Title>PRODUCT LIST</Title>
             <Table>
               <thead>
                 <tr>
                   <th>ID</th>
                   <th>Name</th>
                   <th>Description</th>
                   <th>Price</th>
                   <th>Action</th>
                 </tr>
               </thead>
               <tbody>
                 {products.map((product) => (
                   <tr key={product._id} className={product.isActive ? "" : "muted"}>
                     <td>{product._id}</td>
                     <td>{product.name}</td>
                     <td>{product.description}</td>
                     <td>P{product.price}</td>
                     <td>
                     	<StyledButton onClick={() => handleUpdate(product._id)}>Update</StyledButton>
                    	<StyledButton onClick={() => handleDeactivate(product._id)}>Deactivate</StyledButton>
                    	<StyledButton onClick={() => handleReactivate(product._id)}>Reactivate</StyledButton> 
                     </td>
                   </tr>
                 ))}
               </tbody>
             </Table>
           </Col>
         </Row>
       </Wrapper>
     </Container>
     <Footer />
     <Footers />
     </div>
   );
}


