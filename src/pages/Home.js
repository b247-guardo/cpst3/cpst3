import React from 'react';
import Announcement from '../components/Announcement';
import Slider from '../components/Slider';
import Categories from '../components/Categories';
import Footer from '../components/Footer';
import Footers from '../components/Footers';

export default function Home() {
	return (
		<>
			<Announcement />
			<Slider />
			<Categories />
			<Footer />
			<Footers />
		</>
	)
}