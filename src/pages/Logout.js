import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

export default function Logout(){
	//localStorage.clear();

	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	// Placing the "setUser" setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render different components.
	// By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
	unsetUser();

	useEffect(() => {
		setUser({id: null});
	});

	return(
		<Navigate to="/" />
	)
}