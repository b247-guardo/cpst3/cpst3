import React, { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Footers from '../components/Footers';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const Container = styled.div`
  width: 100%;
  height: auto;
  padding: 50px;
  background: url("https://wallpapercave.com/wp/wp8379188.jpg") center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;  
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #fcf5f5;
  width: 80%;
  height: auto;
  padding: 50px;
`;

const Title = styled.h2`
  font-size: 32px;
  font-weight: 700;
  margin-bottom: 20px;
`;

const Description = styled.p`
  font-size: 20px;
  margin-bottom: 20px;
  text-align: justify;
`;

const Price = styled.p`
  font-size: 24px;
  margin-bottom: 20px;
`;

const AddToCartButton = styled.button`
  background-color: #fff;
  border: 2px solid #000;
  color: #000;
  font-size: 16px;
  padding: 10px 20px;
  cursor: pointer;
  transition: all 0.3s ease;
  margin-top: 20px;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

const QuantityContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

const QuantityLabel = styled.label`
  font-size: 20px;
  margin-right: 10px;
`;

const QuantityInput = styled.input`
  width: 50px;
  font-size: 20px;
  text-align: center;
`;

const TotalAmount = styled.p`
  font-size: 24px;
  margin-bottom: 20px;
`;


export default function ProductDetails() {
  const { id } = useParams();
  const { user } = useContext(UserContext);
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then(response => response.json())
      .then(data => setProduct(data))
      .catch(error => console.error(error));
  }, [id]);
  
  const handleAddToCart = () => {
    if (!product || user.isAdmin) {
      Swal.fire({
        title: "Access Denied!",
        icon: "error",
        text: "We're sorry, but your account does not have the necessary privileges to checkout. Please log in as a regular user or contact customer support for assistance."
      })
      return;
    }

    let cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    const index = cartItems.findIndex((item) => item._id === product._id);
    const newQuantity = parseInt(quantity);

    if (index !== -1) {
      cartItems[index].quantity += newQuantity;
      cartItems[index].total = cartItems[index].quantity * cartItems[index].price;
    } else {
      cartItems.push({...product, quantity: newQuantity, total: product.price * newQuantity});
    }

    localStorage.setItem('cartItems', JSON.stringify(cartItems));
    navigate('/cart');
  };

  const handleQuantityChange = (event) => {
    setQuantity(event.target.value);
  };

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Container>
        <Wrapper>
          <Title>{product.name}</Title>
          <Description>{product.description}</Description>
          <Price>Price: P{product.price}</Price>
          <QuantityContainer>
            <QuantityLabel>Quantity:</QuantityLabel>
            <QuantityInput type="number" min="1" max="10" value={quantity} onChange={handleQuantityChange} />
          </QuantityContainer>
          <TotalAmount>Total: P{product.price * quantity}</TotalAmount>
          <AddToCartButton onClick={handleAddToCart}>Add to Cart</AddToCartButton>
        </Wrapper>
      </Container>
      <Footers />
    </div>
  );
}

