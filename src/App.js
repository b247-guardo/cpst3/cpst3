import Home from './pages/Home';
import AppNavbar from './components/AppNavbar';
import Announcement from './components/Announcement';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ForgotPassword from './pages/ForgotPassword';
import ProductCatalog from './pages/ProductCatalog';
import ProductDetails from './pages/ProductDetails';
import Cart from './pages/Cart';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';
import RetrieveAllProducts from './pages/RetrieveAllProducts';
import UpdateProduct from './pages/UpdateProduct';
import ActiveProduct from './pages/ActiveProduct';
import Error from './pages/Error';
import './App.css';

import { useState, useContext, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { CartProvider } from './CartContext';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Navigate } from 'react-router-dom';

function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <CartProvider>
    <Router>
      <AppNavbar />
        <Container>
          <Routes>
            {user.id === null && <Route path="/" element={<Login/>} />}
            {user.id !== null && <Route path="/" element={<Home/>} />}
            <Route path="/register" element={<Register/>} />
            <Route path="/forgotpassword" element={<ForgotPassword/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/products" element={<ProductCatalog/>} />
            <Route path="/product/:id" element={<ProductDetails/>} />
            <Route path="/cart" element={<Cart/>} />
            <Route path="/admin-dashboard" element={<AdminDashboard />} />
            <Route path="/admin/create-product" element={<CreateProduct />} />
            <Route path="/admin/retrieve" element={<RetrieveAllProducts />} />
            <Route path="/admin/update/:id" element={<UpdateProduct />} />
            <Route path="/active-product" element={<ActiveProduct />} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
      </Router>
      </CartProvider>
    </UserProvider>
    </>
  );
}

export default App;
